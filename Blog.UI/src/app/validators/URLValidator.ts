import { AbstractControl, ValidatorFn } from "@angular/forms";





export function imageUrlValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        // If the control value is empty, consider it valid (you can adjust this based on your requirement)
        return null;
      }
  
      // Regular expression to check if the URL ends with a common image extension
      const imageRegex = /\.(jpeg|jpg|gif|png|svg)$/;
  
      if (!imageRegex.test(control.value.toLowerCase())) {
        return { invalidImageUrl: true };
      }
  
      // URL seems to have a valid image extension
      return null;
    };
  }