import { Component, OnInit } from '@angular/core';
import { PostService } from '../Services/post.service';
import { Post } from '../Models/Post';
import { MatDialog } from '@angular/material/dialog';
import { PostComponent } from '../pages/post/post.component';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  providers: [PostService]
})
export class PostListComponent implements OnInit {
  constructor(private postService: PostService,private dialog:MatDialog ) {

  }
  posts?:Post[];

  ngOnInit(): void {
   this.postService.getAllPosts().subscribe(response =>{
    this.posts = response;
    console.log(this.posts)
  })
  }
  handleImageError(event: Event){
    const imageElement = event.target as HTMLImageElement;
    if(imageElement){
      imageElement.src = 'assets/images/download.png';
    }
  }
  openPost(post:Post){
    const dialogRef = this.dialog.open(PostComponent,{
      // autoFocus: false,
      // hasBackdrop: false,
      // closeOnNavigation: false,
      enterAnimationDuration: '500ms',
      exitAnimationDuration: '500ms',
      width: "60%",
     height:"80%",
      data:post,
    })

  }
}

