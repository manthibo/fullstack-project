export interface Post {
    id: number;
    title: string;
    imageUrl: string;
    content: string;
    created: Date;
}