import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Post } from 'src/app/Models/Post';
import { PostService } from 'src/app/Services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  selectedPost!: Post
  constructor(@Inject(MAT_DIALOG_DATA) public data:any ) {
    this.selectedPost = data;
   }

  ngOnInit(): void { }
  handleImageError(event: Event){
    const imageElement = event.target as HTMLImageElement;
    if(imageElement){
      imageElement.src = 'assets/images/download.png';
    }
  }


}
