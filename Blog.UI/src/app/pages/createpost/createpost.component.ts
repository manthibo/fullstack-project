import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { PostService } from 'src/app/Services/post.service';
import { imageUrlValidator } from 'src/app/validators/URLValidator';

@Component({
  selector: 'app-createpost',
  templateUrl: './createpost.component.html',
  styleUrls: ['./createpost.component.css']
})
export class CreatepostComponent implements OnInit {

  showErrror: boolean = false;
  public Editor = ClassicEditor;

  isLoading = false;
  responseData: any;
  postForm !: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private postService: PostService, @Inject(MAT_DIALOG_DATA) public edit: any,
    private dialogRef: MatDialogRef<CreatepostComponent>) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      imageUrl: ['',[ Validators.required, imageUrlValidator()]],
      content: ['', Validators.required]
    })
    this.isLoading = false;
  }

  addPost(event:Event) {
    event.preventDefault();
    if (this.postForm.valid) {
      this.showErrror = false;
      this.isLoading = true;
      this.postService.addPost(this.postForm.value)?.subscribe(result => {
     this.isLoading = false;
     this.postService.getAllPosts();
     this.dialogRef.close(true)
      })
    }else{
      this.showErrror = true;
     this.isLoading = false;

    }

  }


}
