import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Subject, take, takeUntil } from 'rxjs';
import { PostService } from 'src/app/Services/post.service';
import { imageUrlValidator } from 'src/app/validators/URLValidator';

@Component({
  selector: 'app-editpost',
  templateUrl: './editpost.component.html',
  styleUrls: ['./editpost.component.css']
})
export class EditpostComponent implements OnInit {
  isLoading = false;
  showError: boolean = false;
  editForm!: FormGroup;
  public Editor = ClassicEditor;
  destroy$: Subject<void> = new Subject<void>();


  constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<EditpostComponent>, private postSerive: PostService) { }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      title: ['', Validators.required],
      imageUrl: ['', [Validators.required, imageUrlValidator()]],
      content: ['', Validators.required],
    });
    this.postSerive.postData$.pipe(takeUntil(this.destroy$)).subscribe((postDetails) => {
      if (postDetails) {
        this.editForm.patchValue(postDetails);
      }
    })
  }

  editPost() {
    if (this.editForm.valid) {
      this.postSerive.postData$.pipe(take(1)).subscribe((postDetails) => {
        if (postDetails) {
          const postId = postDetails.id;
          const formData = this.editForm.value;
          this.showError = false;
          this.isLoading = true;
          this.postSerive.updatePost(formData, postId).subscribe((updatedPost) => {
            this.isLoading = false;
            this.postSerive.getAllPosts();
            this.dialogRef.close(true);
          },
            (error) => {
              console.error('Error editing post:', error);
            }
          )
        }
      })

    } else {
      this.showError = true;
      this.isLoading = false;
    }

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}


