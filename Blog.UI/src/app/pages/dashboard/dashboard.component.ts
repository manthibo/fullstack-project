import { Component, OnInit, ViewChild, createComponent } from '@angular/core';
import { Post } from 'src/app/Models/Post';
import { PostService } from 'src/app/Services/post.service';
import { CreatepostComponent } from '../createpost/createpost.component';
import { MatDialog } from '@angular/material/dialog'
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { EditpostComponent } from '../editpost/editpost.component';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: []
})
export class DashboardComponent implements OnInit {
  faPenToSquare = faPenToSquare;
  posts !: Post[];
  displayedColumns: string[] = ['id', 'title', 'imageUrl', 'date', 'action'];

  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private postService: PostService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllPosts()
  }
  getAllPosts() {
    this.postService.getAllPosts().subscribe(response => {
      this.posts = response;
      this.dataSource = new MatTableDataSource<Post>(this.posts)
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort;

    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  handleImageError(event: Event){
    const imageElement = event.target as HTMLImageElement;
    if(imageElement){
      imageElement.src = 'assets/images/download.png';
    }
  }

  openEditForm(postDetails: any) {
    this.postService.setPostData(postDetails)
    const dialogRef = this.dialog.open(EditpostComponent, {
      autoFocus: false,
      hasBackdrop: false,
      closeOnNavigation: false,
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '500ms',
      width: "60%",
      data: postDetails,
    });
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        this.getAllPosts();
      }
    })
  }
  deletePost(id: number) {
    this.postService.deletePost(id).subscribe({
      next: (res) => {
        alert("Post Deleted");
        this.getAllPosts();
      },
      error: () => {
        alert("Error while deleting post")
      }
    })

  }

  openCreateForm() {

    const dialogRef = this.dialog.open(CreatepostComponent, {
      autoFocus: false,
      hasBackdrop: false,
      closeOnNavigation: false,
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '500ms',
      width: "60%",
    });
    dialogRef.afterClosed().subscribe(val => {
      if (val) {
        this.getAllPosts();
      }
    })
  }
}

