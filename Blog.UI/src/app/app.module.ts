import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { MainHeaderComponent } from './shared/main-header/main-header.component';
import { PostListComponent } from './post-list/post-list.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import {MaterialModule} from './material.module';
import { CreatepostComponent } from './pages/createpost/createpost.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostService } from './Services/post.service';
import { EditpostComponent } from './pages/editpost/editpost.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostComponent } from './pages/post/post.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainHeaderComponent,
    PostListComponent,
    DashboardComponent,
    CreatepostComponent,
    EditpostComponent,
    PostComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularEditorModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    CKEditorModule,
    FontAwesomeModule,

  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
