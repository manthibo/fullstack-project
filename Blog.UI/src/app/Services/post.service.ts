import { Injectable } from '@angular/core';
import { Post } from '../Models/Post';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject, map, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private postDataSubject = new BehaviorSubject<any>(null);
  postData$ = this.postDataSubject.asObservable();
  
  private url = "https://localhost:7186"
  private apiUrl = 'https://localhost:7186/api/Post';

  constructor(private http: HttpClient) { }
  setPostData(postData:any)
  {
    this.postDataSubject.next(postData);
  }

  getAllPosts(){
    return this.http.get<Post[]>('https://localhost:7186/api/Post');
  }

  addPost(post:Post){
    if(post.title === " "|| post.content === " "||post.imageUrl === " "){
      return ;
    }
    
    post.created = new Date(Date.now());
    return this.http.post( 'https://localhost:7186/api/Post',post,{responseType: 'text'});
    
  }

  updatePost(post:Post,postId:number){
    post.created = new Date(Date.now());
    const url = `${this.url}/postId:int?id=${postId}`;
    return this.http.put(url,post)
    
  }
  deletePost(postId:number){
    const url = `${this.apiUrl}/${postId}`;
    return this.http.delete(url);
  }

}
