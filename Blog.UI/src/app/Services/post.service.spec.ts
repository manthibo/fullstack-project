import { PreloadingStrategy } from "@angular/router";
import { Post } from "../Models/Post";
import { PostService } from "./post.service"


describe("PostService", () => {
  it("should return a list of posts", () => {

    // Arrange
    const postService = new PostService();
    const posts = postService.Posts;
    // Act
    const result = postService.GetAllPosts();
    // Assert
    expect(result).toBe(posts);
  });

   it("should add post to the list of posts", ()=>{
     // Arrange
     const postService = new PostService();
     const postsList = postService.Posts;
     const post:Post = {
       title : "post title",
       imageUrl: "url",
       content:"content",
       created:new Date()
     }
      // Act
     postService.addPost(post);
     const updatedPostList =postService.Posts;

    // Assert
    expect(updatedPostList).toContain(post)

   });

  it("Should return false when adding an invalid post to list", () => {
     // Arrange
    const postService = new PostService();
    const post:Post = {
      title : " ",
      imageUrl: "url",
      content:"content",
      created:new Date()
    }
     // Act
    const res = postService.addPost(post);
     // Assert
    expect(res).toBeFalse();
  });
});