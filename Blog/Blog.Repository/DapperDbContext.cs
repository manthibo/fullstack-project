﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Blog.Repository
{
    public class DapperDbContext : IDapperDbContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public DapperDbContext(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._connectionString = _configuration.GetConnectionString("connection");
        }

        public IDbConnection CreateConnection()
        {
            return new SqlConnection(this._connectionString);
        }

    }
}
