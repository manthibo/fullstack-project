﻿using Blog.Domain.Models;
using Dapper;
using System.Data;

namespace Blog.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly DapperDbContext _dbContext;
        public PostRepository(DapperDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> CreatePost(Post post)
        {
            string query = "sp_CreatePost";

            using (var connection = this._dbContext.CreateConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@Title", post.Title);
                parameters.Add("ImageUrl", post.ImageUrl);
                parameters.Add("@Content", post.Content);
                parameters.Add("@Created", post.Created);

                return await connection.ExecuteAsync(query, parameters, commandType: CommandType.StoredProcedure) > 0;
            }
        }

        public async Task<List<Post>> GetAllPosts()
        {
            string query = "sp_GetAllPosts";
            using (var connection = this._dbContext.CreateConnection())
            {
                var postList = await connection.QueryAsync<Post>(query, commandType: CommandType.StoredProcedure);

                return postList.ToList();
            }
        }

        public async Task<bool> UpdatePost(Post post, int id)
        {
            string query = "UpdatePost";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            parameters.Add("@Title", post.Title);
            parameters.Add("@ImageUrl", post.ImageUrl);
            parameters.Add("@Content", post.Content);


            using (var connetion = this._dbContext.CreateConnection())
            {
                return await connetion.ExecuteAsync(query, parameters, commandType: CommandType.StoredProcedure) > 0;
            }
        }
        public async Task<bool> DeletePost(int id)
        {
            string query = "sp_DeletePost";
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);

            using (var connetion = this._dbContext.CreateConnection())
            {
                return await connetion.ExecuteAsync(query, parameters, commandType: CommandType.StoredProcedure) > 0;
            }
        }
    }
}
