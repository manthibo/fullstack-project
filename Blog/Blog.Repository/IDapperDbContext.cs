﻿using System.Data;

namespace Blog.Repository
{
    public interface IDapperDbContext
    {
        IDbConnection CreateConnection();
    }
}