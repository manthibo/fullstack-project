﻿using Blog.Domain.Models;

namespace Blog.Repository;

public interface IPostRepository
{
    Task<bool> CreatePost(Post post);
    Task<List<Post>> GetAllPosts();
    Task<bool> UpdatePost(Post post, int id);
    Task<bool> DeletePost(int postId);
}
