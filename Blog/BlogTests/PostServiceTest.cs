﻿using Blog.Domain.Models;
using Blog.Repository;
using Blog.Service;
using FluentValidation;
using NSubstitute;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace BlogTests
{
    public class PostServiceTest
    {
        private IPostService _postService;
        IPostRepository _postRepository;

        [SetUp]
        public void SetUp()
        {

            _postRepository = Substitute.For<IPostRepository>();

            _postService = new PostService(_postRepository);

        }

        [Test]
        public async Task GIVEN_ListOfPosts_WHEN_gettingListOfPosts_THEN_returnListOfUsers()
        {
            //Arrange
            var expextedPosts = new List<Post>
            {
                new Post {Title= "Test", ImageUrl = "test",Content="test",Created= DateTime.UtcNow},
                new Post {Title= "Tes", ImageUrl = "test",Content="test",Created= DateTime.UtcNow}
            };
            _postRepository.GetAllPosts().Returns(expextedPosts);

            //Action
            var result = await _postService.GetPosts();

            //Assert
            Assert.AreEqual(expextedPosts, result);
        }

        [Test]
        public async Task GIVEN_ValidPost_WHEN_CreatingAPost_THEN_returnTrue()
        {
            //Arrange
            var expextedPost = new Post
            {
                Title = "Test",
                ImageUrl = "test",
                Content = "test",
                Created = DateTime.UtcNow,
            };

            _postRepository.CreatePost(expextedPost).Returns(true);

            //Action
            var result = await _postService.CreatePost(expextedPost);

            //Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task GIVEN_InavlidPosts_WHEN_CreatingAPost_THEN_returnFalse()
        {
            //Arrange
            var invalidPost = new Post
            {
                Title = "Test",
                ImageUrl = "",
                Content = "",
                Created = DateTime.UtcNow,
            };
            var expectedresults = new List<Error>
            {
               new Error{ErrorMessage ="Image Url is required"},
               new Error{ErrorMessage = "Content is required"}
            };

            //Action
            var results = await Assert.ThrowsExceptionAsync<ValidationException>(async () => await _postService.CreatePost(invalidPost));

            //Assert.AreEqual("Content is required", expectedresults[1]);
            // Assert.IsInstanceOfType(results.Errors, typeof(List<Error>));
            //Assert.IsInstanceOfType(expectedresults, typeof(List<Error>));
            // Assert.AreEqual(results.Errors.ToList(), expectedresults);
            Assert.IsInstanceOfType(results, typeof(ValidationException));
            Assert.IsInstanceOfType(invalidPost, typeof(Post));
        }

        [Test]
        public async Task Given_AValidPost_WHEN_UpdatingAPost_THEN_ReturnTrue()
        {
            //Arrage
            var validPost = new Post
            {
                Title = "Test",
                ImageUrl = "Test Update",
                Content = "Test",
                Created = DateTime.UtcNow,
            };
            int postId = 2;

            _postRepository.UpdatePost(validPost, postId).Returns(true);

            //Action
            var result = await _postService.UpdatePost(validPost, postId);

            //Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task Given_invalidPost_WHEN_updatingAPost_THEN_returnsBadREquest()
        {
            //Arrange
            var invalidData = new Post
            {
                Title = "Test",
                ImageUrl = "",
                Content = "Test ",
                Created = DateTime.UtcNow
            };
            int invalidPostId = 2;


            //Action
            var results = await Assert.ThrowsExceptionAsync<ValidationException>(async () => await _postService.UpdatePost(invalidData, invalidPostId));

            //Assert
            Assert.AreEqual("Image Url is required", results.Errors.First().ErrorMessage);
            Assert.IsInstanceOfType(results, typeof(ValidationException));

        }

        [Test]
        public async Task Given_invalidPostData_WHEN_updatingAPost_THEN_returnsBadREquest()
        {
            //Arrange
            var invalidPostData = new Post
            {
                Title = "Test",
                ImageUrl = "hytg",
                Created = DateTime.UtcNow
            };
            int invalidPostId = 2;

            //Action
            var results = await Assert.ThrowsExceptionAsync<ValidationException>(async () => await _postService.UpdatePost(invalidPostData, invalidPostId));

            //Assert
            Assert.AreEqual("'Content' must not be empty.", results.Errors.First().ErrorMessage);
            Assert.AreEqual("Content is required", results.Errors.ToList()[1].ErrorMessage);
            Assert.IsInstanceOfType(results, typeof(ValidationException));

        }
        [Test]
        public async Task Given_postID_WHEN_deletingPost_THEN_ResturnsOKResults()
        {
            //Arrange
            var postId = 1;

            _postRepository.DeletePost(postId).Returns(true);

            //Action
            var result = await _postService.DeletePost(postId);

            //Asser
            Assert.IsTrue(result);

        }

        [Test]
        public async Task GIVEN_InvalidPostD_WHEN_deletingPost_THEN_ReturnsNotFound()
        {
            //Arrange
            var invalidpostId = 1;
            _postRepository.DeletePost(invalidpostId).Returns(false);

            //Action
            var result = await _postService.DeletePost(invalidpostId);

            //Assert
            Assert.IsFalse(result);
        }
    }
}

