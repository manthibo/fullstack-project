﻿using Blog.Domain.Models;
using Blog.Repository;
using Blog.Service.Validators;
using FluentValidation;

namespace Blog.Service
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IValidator<Post> _postValidator = new PostValidator();
        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;

        }

        public Task<bool> CreatePost(Post post)
        {
            var validationResult = _postValidator.Validate(post);

            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }
            return _postRepository.CreatePost(post);
        }

        public Task<bool> DeletePost(int postId)
        {
            return _postRepository.DeletePost(postId);
        }

        public Task<List<Post>> GetPosts()
        {
            return _postRepository.GetAllPosts();
        }

        public Task<bool> UpdatePost(Post post, int id)
        {
            var validationResult = _postValidator.Validate(post);
            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }
            return _postRepository.UpdatePost(post, id);
        }

    }
}
