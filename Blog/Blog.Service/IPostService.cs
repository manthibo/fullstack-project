﻿using Blog.Domain.Models;

namespace Blog.Service
{
    public interface IPostService
    {
        Task<bool> CreatePost(Post post);
        Task<List<Post>> GetPosts();
        Task<bool> UpdatePost(Post post, int postId);
        Task<bool> DeletePost(int postId);

    }
}
