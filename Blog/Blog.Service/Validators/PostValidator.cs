﻿using Blog.Domain.Models;
using FluentValidation;

namespace Blog.Service.Validators
{
    public class PostValidator : AbstractValidator<Post>
    {
        public PostValidator()
        {
            RuleFor(u => u.Title).NotNull().NotEmpty().WithMessage("Title is required");
            RuleFor(u => u.ImageUrl).NotNull().NotEmpty().WithMessage("Image Url is required");
            RuleFor(u => u.Content).NotNull().NotEmpty().WithMessage("Content is required");
            RuleFor(u => u.Created).NotNull().NotEmpty().WithMessage("Date is reuired");
        }
    }
}
