﻿using Blog.Domain.Models;
using Blog.Service;
using Microsoft.AspNetCore.Mvc;


namespace Blog.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            this._postService = postService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CreatePost([FromBody] Post post)
        {
            try
            {
                var result = await this._postService.CreatePost(post);
                return StatusCode(StatusCodes.Status200OK, "Request completed successfully.");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Inavlid request");
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllPosts()
        {
            try
            {
                var list = await this._postService.GetPosts();
                if (list != null)
                {
                    return Ok(list);
                }
                else
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No resources found.");
                }
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "An internal server error occurred.");
            }
        }

        [HttpPut("/postId:int")]
        public async Task<IActionResult> UpdatePost([FromBody] Post post, int id)
        {
            try
            {
                var result = await this._postService.UpdatePost(post, id);
                return Ok(result)
;
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An internal server error occurred.");

            }
        }
        [HttpDelete("{postId}")]
        public async Task<IActionResult> DeletePost(int postId)
        {
            try
            {
                var deleted = await _postService.DeletePost(postId);

                if (deleted)
                {
                    return NoContent();
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Post was not found.");
                }

            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "An internal server error occurred.");
            }
        }

    }
}
