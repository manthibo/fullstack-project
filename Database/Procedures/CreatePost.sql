﻿CREATE PROCEDURE sp_CreatePost

@Title VARCHAR(150),
@ImageUrl VARCHAR(150),
@Content VARCHAR(MAX),
@Created DATETIME

AS
BEGIN
	INSERT INTO Post_Table(Title,ImageUrl,Content,Created)
	VALUES(@Title,@ImageUrl,@Content,@Created)
END