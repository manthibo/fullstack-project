
CREATE PROCEDURE sp_UpdatePost
@Id int,
@Title VARCHAR(150),
@ImageUrl VARCHAR(150),
@Content  VARCHAR(MAX),
@Created DATETIME

AS
BEGIN
	UPDATE Post_Table 
	SET Title = @Title,
		ImageUrl = @ImageUrl,
		Content = @Content,
		Created = @Created
	WHERE Id = @Id
END

